package com.example.foodresturant;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gotoHome(View view){
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        startActivity(intent);
        }
    public void gotoWaiter(View view){
        Intent intent = new Intent(MainActivity.this, waiter.class);
        startActivity(intent);
    }
//    public void gotoqrCode(View view){
//        Intent intent = new Intent(MainActivity.this, qr_code.class);
//        startActivity(intent);
//    }
    public void gotoMenu(View view){
        Intent intent = new Intent(MainActivity.this, menu.class);
        startActivity(intent);
        }
    public void gotoLocation(View view){
        Intent intent = new Intent(MainActivity.this, location.class);
        startActivity(intent);
    }
    public void gotoComments(View view){
        Intent intent = new Intent(MainActivity.this, comments.class);
        startActivity(intent);
    }

    public void gotoqrCode(View view){
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }
    @Override
    protected void onPause(){
        super.onPause();
       // mScannerView.stopCamera();

    }
    @Override
    public void handleResult(Result result) {
    // Do anything with qrcode result here :D
        Log.v("handleResult", result.getText());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan Result");
        builder.setMessage(result.getText());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        //Resume Scanning
        mScannerView.resumeCameraPreview(this);
    }


//    public void init(){
//        btn = (Button)findViewById(R.id.button1);
//        btn.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MainActivity.this, qr_code.class);
//                startActivity(intent);
//            }
//
//            @Override

//        });
//    }



}